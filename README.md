# Cms and account #

### Project includes: ###

- Django 1.9.*
- Django cms
- User account
- Multilanguage

----------


### Usage: ###

    django-admin.py startproject --template=https://bitbucket.org/bezobzira/django-project19/get/master.zip <project_name>

----------

### Installation: ###

    pip install virtualenv
    virtualenv mysite-env
    source mysite-env/bin/activate
    django-admin.py startproject --template=https://bitbucket.org/bezobzira/django-project19/get/master.zip <project_name>
    cd mysite
    pip install -r requirements/requirements.txt / requirements/windows/requirements.txt
	manage.py makemigrations (after adding languages / needed for modetranslation)
    manage.py migrate
    manage.py runserver

----------

### Extra files ###


#### Settings ####

- settings.py (optimized)
- local_settings.py
- urls.py


#### _local_settings.py_: ####

Here you can add:

- Database (name of DB equals to project name)
- Email data

----------
#### Templates ####

- `base.html`
- `_account_bar.html`
- `_footer.html`
- Admin
	- Custom admin templates (fwd support header)
- Menu
	- `breadcrumb.html`
	- `dummy.html`
	- `empty.html`
	- `language_chooser.html`
	- `menu.html`
	- `menu_bootstrap.html`
	- `sub_menu.html`
- Layouts
	- `content_with_slider.html`
	- `content_without_slider.html`
	- `home.html`
	- `location.html`
	- extensions
		- `slider.html`


----------


### Requirements included: ###

Django==1.9.*
MySQL-python
Pillow
Unidecode
django-admin-sortable2
django-appconf
django-appdata
django-bootstrap-form
django-ckeditor
django-classy-tags
django-cms
django-el-pagination
django-filer
django-jsonfield
django-modeltranslation
django-mptt
django-polymorphic
django-sekizai
django-treebeard
django-user-accounts
djangocms-file
djangocms-link
djangocms-picture
djangocms-snippet
djangocms-text-ckeditor
djangocms-video
djangocms-teaser
cmsplugin-contact
cmsplugin-filer
easy-thumbnails
recaptcha-client
eventlog
html5lib
metron
pinax-theme-bootstrap
pytz
six
unicode-slugify
wheel
wsgiref
yolk






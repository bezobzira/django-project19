require('../../node_modules/owl-carousel-2/owl.carousel');

module.exports = {
	init: function(){
		$('.js-owl-carousel').owlCarousel({
			loop: true,
			nav: true,
			dots: true,
			autoplay: true,
			autoplayHoverPause: true,
			responsive: {
				 0:{
					items:1
				},
				600:{
					items:1
				},
				1000:{
					items:1
				}
			}
		});
		$('.js-owl-carousel-hero').owlCarousel({
			loop: true,
			autoplay: true,
			autoplayTimeout: 8000,
			autoplayHoverPause: false,
			items: 1,
			margin: 0,
			nav: false,
			dots: false,
			animateOut: 'fadeOut',
			animateIn: 'fadeIn',
			responsive: {
				 0:{
					items:1
				},
				600:{
					items:1
				},
				1000:{
					items:1
				}
			}
		});
	}
};
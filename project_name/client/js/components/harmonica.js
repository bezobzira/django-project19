require('../../node_modules/owl-carousel-2/owl.carousel');

module.exports = {
	init: function(){
		$('.js-harmonica').each(function(){
			var $harmonica = $(this);

			$harmonica.find('.harmonica__trigger').click(function(){
				var $li = $(this).parent(),
					is_active_clicked = $li.hasClass('active'),
					$active = $harmonica.find('.active');

				$active.find('.harmonica__content').animate({'height':0}, 700);
				$active.removeClass('active');

				if(is_active_clicked) return;

				var $new_active_content = $li.find('.harmonica__content');
				$new_active_content.css({'height':'auto'});
				var h = $new_active_content.height();
				$new_active_content.css({'height':'0px'});
				$new_active_content.animate({'height':h});
				$li.addClass('active');
			});

		});

	}
};
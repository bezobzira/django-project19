module.exports = {
	init: function(){
		var menuWrap = $('.menu-fourth-level'),
			trigger = menuWrap.children('.note'),
			menu = menuWrap.children('.menu-inner');

		trigger.click(function () {
			menuWrap.toggleClass('active');
			menu.slideToggle();
		});

	}
};
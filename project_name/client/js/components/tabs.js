require('../../node_modules/owl-carousel-2/owl.carousel');

module.exports = {
	init: function(){
		$('.js-tabs').each(function(){
			var $tabs = $(this);

			$tabs.find('.tabs__nav li').click(function(){
				var $li = $(this);
				$li.siblings().removeClass('active');
				$li.addClass('active');

				$tabs.find('.tabs__content__item.active').removeClass('active');
				$tabs.find('.tabs__content__item:eq('+$li.index()+')').addClass('active');
			});

			$tabs.find('.tabs__nav li:eq(0)').trigger('click');

		});

	}
};
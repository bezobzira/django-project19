// Make jQuery global if not
window.jQuery = window.jQuery ? window.jQuery : require("../node_modules/jquery/jquery.min");
window.$ = window.$ ? window.$ : window.jQuery;
window.jQuery = window.$;


// INIT THAT SHIT!
$(function() {
	var carousels = require('./components/carousels'),
		fancybox = require('./components/fancybox'),
		tabs = require('./components/tabs'),
		harmonica = require('./components/harmonica'),
		menu4 = require('./components/menu4');

	carousels.init();
	fancybox.init();
	tabs.init();
	harmonica.init();
	menu4.init();
});